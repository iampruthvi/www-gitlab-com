---
layout: markdown_page
title: "CircleCI for the Business Decision Maker"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## CircleCI Strengths
 
* YAML File:  CI build jobs are configured in a YAML file
* Security Accreditations: CircleCI is FedRamp authorized and SOC 2 compliant
* Forrester Wave: CircleCI was recognized as a Leader in The Forrester Wave™: Cloud-Native Continuous Integration Tools, Q3 2019: speed, scale, security, and compliance.
* Catering to Large Enterprise:
   - CircleCI provides a large number of preconfigured environments, which is highly favored by enterprises
   - Despite a team size of just over 300 employees, CircleCI has a Customer Success Team and Enterprise support packages, also favored by enterprises
* iOS application testing on macOS: CircleCI offers support for building and testing iOS projects in macOS virtual machines (available on CircleCI Cloud only, not currently available on self-hosted installations).  
   - GitLab is actively working on integrating this functionality, more details can be found [here](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/5720)
   
## CircleCI Limitations and Challenges


* Free Plan Trap: Their free plan offers a generous amount of free credits, 2,500/week.  However, these credits are used to pay for CI run times across medium sized Linux and Windows machines only; and are used for Orb usage, Workspaces and Dependency Caching. 
* No Single Integrated DevOps Application: CircleCI is a tool that automates the Continuous Integration stage of the Software Development Life Cycle.  To extend the functionality beyond CI, integration with third party plugins is required.  Plugins are expensive to maintain, secure, and upgrade. In contrast, GitLab is [open core](https://about.gitlab.com/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change.
* Missing Enterprise Features: CircleCI lacks native support for key enterprise features such as Feature Flags, Kubernetes Support and Canary Deployments.
* Hybride CI: CircleCI lacks the ability to orchestrate a customers private server CI builds with their cloud hosted CI server

## GitLab Strengths over CircleCI

|         GitLab Differentiator        |                                                                                                                                                                                                                Why Is This Important?                                                                                                                                                                                                                |
|:------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| Intuitive and Customizable Dashboard | CircleCI customers have reported to Forrester analyst the desire to have a more customizable UI experience when using CircleCI                                                                                                                                                                                                                                                                                                                       |
| Fully Integrated CI/CD               | Fully integrated, means less context switching between tools and more time delivering software.  CircleCI is a CI tool that uses plugins for CD and other functions                                                                                                                                                                                                                                                                                  |
| Integrated Container Registry        | Fully integrated container registry offers more security, governance and efficient management.  CircleCI does not offer this                                                                                                                                                                                                                                                                                                                         |
| Local Artifact Storage               | Fully integrated artifact storage  offers more security, governance and efficient management.  CircleCI does not offer this, they store Artifacts on Amazon S3.                                                                                                                                                                                                                                                                                      |
| Rich Insights and Analytics          | Detailed analytics and insights helps customers improve software development times.  Aggregating insights and analytics across projects provides users with easy access to valuable information.  GitLab does this well while CircleCI does not.  CircleCI’s on-prem CI server offers very shallow insights.  Their SaaS CI solution provides deeper insights but requires a separate self-managed server and is not integrated into their Cloud UI. |
| Hybrid CI Model                      | More and more customers are attracted to SaaS tools but don’t want to place their source code in the cloud.  For this reason Hybrid CI solutions that combine a cloud-native SaaS CI server with on-premises agents are becoming more desirable because it simplifies tool maintenance while dodging security concerns.                                                                                                                              |
| Single Application                   | Built-in CI/CD transforms how you deliver today and simultaneously prepares your SDLC for evolving needs tomorrow.                                                                                                                                                                                                                                                                                                                                   |
| End-to-End Automation                | Powerful enough alone yet flexible enough to integrate with other tools, GitLab ensures quality code gets to production faster.                                                                                                                                                                                                                                                                                                                      |

## CircleCI vs GitLab Summary

|  GitLab |                                                                            |  CircleCI |
|:-------:|:--------------------------------------------------------------------------:|:---------:|
| YES     |                            Self Hosted and .com                            | YES       |
| YES     |               Ecosystem (https://about.gitlab.com/partners/)               | YES       |
| No      |                                 Marketplace                                | YES       |
| YES     |            CD fully integrated - no 3rd party Plugins/tools need           | No        |
| YES     |                Built in Kubernetes Deployment and Monitoring               | No        |
| YES     |                       Auto CI  Pipeline Configuration                      | No        |
| YES     |      Built in CI Security Scanning - no 3rd party Plugins/tools needed     | No        |
| YES     |           Security Dashboard enabling Security Team collaboration          | No        |
| YES     | Supports Hybrid CI Orchestration Model (SaaS CI Server  and On-Prem Agent) | No        |


