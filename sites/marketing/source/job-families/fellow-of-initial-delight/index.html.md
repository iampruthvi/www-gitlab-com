---
layout: job_family_page
title: "Fellow of Initial Delight"
---

Fellow of Initial Delight is a role at GitLab that ensures the product creates a great first impression.
This role was created by GitLab co-founder Dmitriy Zaporozhets (DZ).
It is a mix of user experience, product, and engineering.
There is a [creator pairing](/handbook/creator/pairing) program that allows engineers at GitLab to temporarily participate by working with DZ.

## Responsibilities

The Fellow of Initial Delight will set up and use GitLab features on regular basis.
Anything that feels broken or simply creates a bad impression should be addressed by an issue or a merge request.

Points of interest can be:
* a broken feature
* a feature not being enabled by default
* confusing or broken UI
* missing or broken link to documentation

The good example of such a finding is GitLab not having [Container registry](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4690) enabled by default.

## Requirements

* Ability to use GitLab

## Performance Indicators

* [Paid Net Promoter Score](/handbook/product/metrics/#paid-net-promoter-score)

## Hiring Process

There is no hiring process outlined for this role at this time.
