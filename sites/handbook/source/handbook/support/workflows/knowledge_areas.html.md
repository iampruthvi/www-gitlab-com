---
layout: handbook-page-toc
title: Support Team Knowledge Areas
category: References
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Overview

The purpose of this page is to provide a reference of Support team members who feel that they are knowledgeable and willing to assist with tickets in a particular area.

Unlike the [team page](/company/team/), team members listed here may or may not be [an expert](/company/team/structure/#expert), such as [working on a bootcamp](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates) but not completed. Many areas however do not currently have a bootcamp, or their knowledge area is very specific, such as "GitLab.com SSO".

### When to Use

Team members should make use of existing resources before making use of this list: docs, ZenDesk, Slack (support, stage/group specific channels, questions).

If no response is forthcoming or you need to find someone to assist, you can consider pinging team members listed below.

When pinging a team member, please consider the [Support Engineer Areas of Focus](/handbook/support/support-engineer-responsibilities.html#support-engineer-areas-of-focus) as some team members may or may not have access to what is being inquired about.

### General Technologies and Topics

Please remember to post in the appropriate Slack channel (such as #kubernetes) and/or check the [team page](/company/team/) for non-support team members who can help as well.

Topics are in alphabetical order with team members grouped by region.

| Technology | Region | Team Member |
| ---------- | ------ | ----------- |
| Docker | AMER | Harish<br>Cody West<br>Caleb W.<br>Davin<br>Lewis |
|  | APAC | Julian<br>Priyan<br>Athar |
|  | EMEA | Catalin<br>Rehab<br>Alin |
| ElasticSearch | AMER | Blair<br>Michael Lussier<br>JasonC<br>Caleb C |
|  | APAC | AlexS |
| Git LFS | AMER | Diana<br>Will<br>Lewis |
|  | EMEA | Ben |
| HA | AMER | Aric<br>James<br>JasonC<br>Gabe<br>Lewis |
|  | APAC | Mike<br>Athar<br>AlexS |
|  | EMEA | Catalin |
| Kubernetes | AMER | Harish<br>JasonC<br>Michael Lussier<br>Thiago<br>Caleb W.<br>JasonY<br>Lewis |
|  | APAC | Alex Tanayno<br>Julian<br>Arihant<br>Priyan |
|  | EMEA | Rehab<br>VladB |
| Linux | AMER | Greg<br>Keven<br>Gabe<br>Cody West<br>James Lopes<br>Will<br>Davin<br>Lewis<br>Tom H<br>Duncan<br>Mario<br>Caleb C. |
|  | EMEA | Ben<br>David Wainaina<br>Catalin<br>Rehab<br>Alin<br>VladB<br>Łukasz |
|  | APAC | Mike<br>Priyan<br>Athar<br>AlexS |
| Omnibus | AMER | Aric<br>Diana<br>Greg<br>Harish<br>John<br>Nathan<br>Gabe<br>Cody West<br>Keven<br>Davin<br>JasonY<br>Lewis<br>Brie<br>Phil |
|  | APAC | AlexS<br>Anton<br>Weimeng<br>Mike<br>Priyan<br>Athar<br>Emily |
|  | EMEA | Catalin<br>Segolene<br>Rehab<br>Alin<br>VladB<br>Joseph<br>Aakif |
| Performance | AMER | Will<br>Cody West |
|  | EMEA | Catalin<br>Alin |
| SSL | AMER | John |

### Scripting Languages

If you need help with scripting for fixes, internal tools, or any other reason.

| Technology | Region | Team Member |
| ---------- | ------ | ----------- |
| Ruby/Rails | AMER | Davin<br>JasonY<br>Lewis<br>Sara |
|  | EMEA | Ronald<br>Catalin |
|  | APAC | Wei-Meng Lee<br>Arihant<br>Priyan<br>Anton<br>Athar |
| GO | APAC | Athar |
|  | EMEA | Catalin |
|  | AMER |  |
| JavaScript | APAC | Wei-Meng Lee<br>Arihant<br>Anton<br>Athar |
|  | EMEA | Ronald<br>Catalin<br>Alin |
|  | AMER | Davin<br>Phil<br>Sara |
| Vue.js | APAC | Wei-Meng Lee<br>Athar |
|  | AMER | Sara |
|  | EMEA | Catalin |
| Ansible | AMER | JasonY |
|  | EMEA | Shem |
|  | APAC | Wei-Meng Lee<br>Julian<br>Mike<br>Priyan<br>AlexS<br>Athar |
| Zendesk app development | EMEA | DeAndre |
| Bash | APAC | Mike<br>Priya<br>AlexS<br>Duncan |
|  | EMEA | David Wainaina<br>Catalin<br>Rehab<br>Alin<br>VladB<br>Łukasz |
|  | AMER | Davin<br>Lewis<br>Caleb C. |
| Python | APAC | Matthew<br>Athar |
|  | EMEA | Catalin<br>Alin<br>Muhamed<br>VladB<br>Łukasz |
|  | AMER | Lewis |

### Product Stages

This section is ordered by stage and group according to the [product categories page](/handbook/product/product-categories/#devops-stages). If no one is listed for an area, look for a Support Counterpart on the product page.

#### Manage

##### Access

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| LDAP | AMER | Blair<br>Diana<br>Harish<br>JasonC |
|  | APAC | Alex Tanayno<br>Athar<br>Matthew<br>Priyan |
| SAML | AMER | Blair<br>Diana<br>JasonC |
|  | APAC | Anton<br>Arihant |
| SSO for Groups | AMER | Cynthia<br>Tristan<br>Arihant |
|  | APAC | Arihant |

##### Import

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Project Import | AMER | Cynthia<br>Sara |
|  | EMEA | Rehab<br>VladB |

#### Plan

##### Project Management

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Issue Tracking | APAC | Arihant |

##### Portfolio Management

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
|  |  |  |

##### Certify

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Service Desk | Amer | John |

#### Create

##### Source Code

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Code Owners | AMER | Tristan<br>Will |
| Repository Mirroring | AMER | Tristan |
|  | EMEA | VladB |

##### Editor

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Snippets | AMER | John |
| Web IDE | AMER | John |

##### Gitaly

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Gitaly | AMER | Will |

##### Ecosystem

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| JIRA Integration | AMER | Tristan<br>Aric<br>Sara |
|  | APAC | Priyan |

#### Verify

##### Continuous Integration

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| CI | EMEA | DeAndre<br>Ronald<br>Silvester<br>David Wainaina<br>Shem<br>VladB |
|  | AMER | Cynthia<br>Harish<br>Cody West<br>Cleveland<br>Gabe<br>Caleb W.<br>Phil<br>Brie |
|  | APAC | Alex Tanayno<br>Athar<br>Arihant<br>Priyan<br>Anton<br>Sameer<br>Sara |
| Jenkins Integration | AMER | Aric<br>Sara |

##### Runner

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| GitLab Runner | APAC | Alex Tanayno<br>Arihant<br>Anton<br>Athar |
|  | EMEA | Ronald<br>Silvester |
|  | AMER | Davin<br>Phil<br>Sara |
| Kubernetes Executor | AMER | Phil |
|  | EMEA |  |
|  | APAC |  |
| Windows | AMER |  |
|  | EMEA |  |
|  | APAC | Anton |

##### Testing

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
|  | AMER |  |
|  | EMEA |  |
|  | APAC |  |

#### Package

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Package | AMER | Thiago<br>Sara<br>Caleb W. |
|  | EMEA |  |
|  | APAC |  |
| Container Registry | AMER | Will<br>Davin<br>Sara |
|  | EMEA |  |
|  | APAC |  |
| Helm Chart Registry | AMER |  |
|  | EMEA |  |
|  | APAC |  |
| Dependency Proxy | AMER |  |
|  | EMEA |  |
|  | APAC | Anton |

#### Release

##### Progressive Delivery

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
|  |  |  |

##### Release Management

| Topic                 | Region | Team Member                                                |
| --------------------- | ------ | ---------------------------------------------------------- |
| Release Orchestration | Amer   | JamesM                                                     |
| Secrets Management    | Amer   | JamesM                                                     |
| Release Evidence      | Amer   | JamesM                                                     |
| GitLab Pages          | Amer   | John <br> Keven <br> JamesM <br> Brie <br> Davin <br> Phil |
|                       | EMEA   | Catalin <br> Rehab <br> Katrin                             |
|                       | APAC   | MikeL                                                      |

#### Configure

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
|  |  |  |

#### Monitor

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Metrics | APAC | Arihant |
| [Monitoring GitLab](https://docs.gitlab.com/ee/administration/monitoring/) | EMEA | Muhamed<br>VladB |

##### Health

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
|  |  |  |

#### Secure

| Group | Topic | Region | Team Member |
| ----- | ----- | ------ | ----------- |
| Static Analysis | SAST | AMER | Thiago<br>Greg |
|  |  | APAC | Anton |
| Dynamic Analysis | DAST | AMER | Thiago<br>Greg |
|  |  | APAC | Anton |
| Composition Analysis | Dependency Scanning<br>Container Scanning<br>License Compliance | AMER | Greg<br>Katrin |

#### Defend

##### Container Security

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| WAF | AMER | Brie |

#### Growth

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| License and Renewals | AMER | Tom H<br>Keven<br>Kaitlyn<br>James M<br>Eric |
|  | APAC | Rotana<br>Sameer |
|  | EMEA | Rene<br>Donique<br>Collen |

#### Enablement

##### Distribution

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Cloud Native / Chart | AMER | JasonY |
|  | EMEA |  |
|  | APAC | Priyan |

##### Geo

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| Geo | EMEA | Alin<br>Catalin<br>Collen |
|  | AMER | Aric<br>Harish<br>John<br>Gabe<br>Lewis |
|  | APAC | Anton<br>AlexS |
| Backup/Restore | EMEA |  |
|  | AMER | Davin<br>Lewis |
|  | APAC | AlexS |

##### Database

| Topic | Region | Team Member |
| ----- | ------ | ----------- |
| PostgreSQL | EMEA | Ben |
|  | AMER | Sara |
|  | APAC | Mike |
| Database Migrations | EMEA | Rehab |
|  | AMER | Sara |
