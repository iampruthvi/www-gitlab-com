---
layout: handbook-page-toc
title: "Infrastructure Analyst"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The responsilities and other info about this role specifically are also available with more detail at the [Cost Management](/handbook/engineering/infrastructure/cost-management/infrastructure-analyst-role) section of our handbook that goes over more specifics related to this role

## About me

I'm Davis Townsend. I'm born and raised in Austin, Texas and I have a a MS in Business Analytics and B.A. in Economics from University of Texas at Austin. I started my career by interning at a local data consulting startup during college, and then worked for a large video game publisher for 2 years after college where I got really into infrastructure cost management before I came here to GitLab.

Outside of work, I like to play video games, sports, and go hiking or snowboarding depending on the season.


## Responsibilities
- Optimize infra spend through committed spend programs and supporting enterprise contract deals
- Ad Hoc Analysis of Infrastructure hosting spend
- Dashboard and chart creation to improve observability into infrastructure costs
- Teaching and partnering with PM's and others on how to understand the cost and usage of their services

## Working with an infrastructure analyst

There is no magic when reducing or analyzing infrastructure spend, it just requires a good understanding of what our own usage and architecture looks like and how we get billed for it.

Therefore, if requesting help or a review from an infrastructure analyst you should try to provide as much information as possible. Some of the most important pieces of information to provide are:
- description or link to architecture of affected services
- data Source of usage info and what metrics best relate to the cost if known
- Affected product and/or sku of service provider

If one or more of these is not known, then you should partner with the infrastructure analyst so you can define these together.

## Contact Me

- post in slack in [#infrafin](https://gitlab.slack.com/messages/infrafin/)
- open issue with infrafin label so it shows up on the [Infrafin Board](https://gitlab.com/groups/gitlab-com/-/boards/1502173?label_name[]=infrafin), it will be groomed and I will resolve it async if it adheres to the [infrafin board criteria](/handbook/engineering/infrastructure/cost-management/infrafin-board/#criteria).
    - If the issue does not meet the criteria, then open the issue wherever it is most appropriate and ping me in the issue, I will work through these issues in a backlog at a lower priority then items on the infrafin board
- I am willing to join any meeting you schedule within my calendar availability as long as it follow's GitLab's general guidelines around sync meetings. If I think the topic of the meeting can be solved just as easily async, I will not take the meeting and instead ask to solve it async to keep the number of meetings I have down.
    


