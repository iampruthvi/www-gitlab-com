- name: Secure and Defend Section - Secure Stage MAU - Unique users who have used a Secure
    scanner
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The number of unique users who have run one or more Secure scanners.
  target: Progressively increasing month-over-month
  org: Secure and Defend Section
  public: true
  pi_type: Section MAU
  telemetry_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  instrumentation:
    level: 2
    reasons:
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Threat Insights data is not yet included in this count as it just moved from
      Defend.
  sisense_data:
    chart: 9282766
    dashboard: 707777
    embed: v2
- name: Secure:Static Analysis - GMAU - Users running SAST
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more SAST
    jobs.
  target: Progressively increasing month-over-month, >10%
  org: Secure and Defend Section
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  telemetry_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  instrumentation:
    level: 2
    reasons:
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
  lessons:
    learned:
      - Large increase in usage of SAST due to the move of SAST to Core and inclusion in Auto DevOps.
      - We're likely still undercounting this as we use an exact match on SAST job names rather than ilike fuzzy match for SAST which will catch more job names. This is being updated. 
  monthly_focus:
    goals:
      - Now that SAST is availible to all plan types, creating a better experience for non-Ultimate users to help them discover value and become interested in upgrading. [See issue](https://gitlab.com/gitlab-org/gitlab/-/issues/242093)
  sisense_data:
    chart: 9284778
    dashboard: 707777
    embed: v2
- name: Secure:Static Analysis - Paid GMAU - Paid users running
    SAST
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more SAST
    jobs and who are Ultimate users.
  target: Progressively increasing month-over-month
  org: Secure and Defend Section
  stage: secure
  group: static_analysis
  public: true
  pi_type: Paid GMAU
  telemetry_type: SaaS
  plan_type: Paid
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  instrumentation:
    level: 2
    reasons:
    - Can't currently differentiate between Core and Ultimate users.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
  lessons:
    learned:
      - Static Analysis has a much better understanding of the data model and specifically what data we have available. 
      - SAST to Core has had little to no effect on downgrades thus far. Upgrade conversations for existing GitLab users is much smoother with trialing SAST now easier to accomplish. 
  monthly_focus:
    goals:
      - We continue to [iterate on SAST Configuration UI](https://gitlab.com/groups/gitlab-org/-/epics/3262) to support more complex configurations and specific analyzer settings to make it easier to get started with SAST.
      - Working to get more data on SAST usage. [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/211621)
      - Trialing some new methods for splitting out plan data. [Draft WIP Static Analysis dashboard](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin)
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Static Analysis - GMAU - Users running Secret Detection
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Secret Detection jobs.
  target: Progressively increasing month-over-month, >10%
  org: Secure and Defend Section
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  telemetry_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  instrumentation:
    level: 2
    reasons:
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
  lessons:
    learned:
      - Large increase in usage of Secret Detection due to the move of Secret Detection to Core and inclusion in Auto DevOps.
      - We're likely still undercounting this as we use an exact match on secret-detection job names rather than ilike fuzzy match for secret-detection which will catch more job names. This is being updated. 
  monthly_focus:
    goals:
      - Now that Secret Detection is availible to all plan types, creating a better experience for non-Ultimate users to help them discover value and become interested in upgrading. [See issue](https://gitlab.com/gitlab-org/gitlab/-/issues/242093)
  sisense_data:
    chart: 9620614
    dashboard: 707777
    embed: v2
- name: Secure:Static Analysis - Paid GMAU - Paid users running
    Secret Detection
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Secret Detection jobs
    and who are Ultimate users.
  target: Progressively increasing month-over-month
  org: Secure and Defend Section
  stage: secure
  group: static_analysis
  public: true
  pi_type: Paid GMAU
  telemetry_type: SaaS
  plan_type: Paid
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  instrumentation:
    level: 2
    reasons:
    - Can't currently differentiate between Core and Ultimate users.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
  lessons:
    learned:
      - Static Analysis has a much better understanding of the data model and specifically what data we have available. 
      - Secret Detection to Core had no effect on downgrades thus far.
  monthly_focus:
    goals:
      - Working to get more data on Secret Detection usage. [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/211621)
      - Trialing some new methods for splitting out plan data. [Draft WIP Static Analysis dashboard](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin)
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Dynamic Analysis - GMAU - Users running DAST
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST jobs. Same as Paid
    GMAU.
  target: Progressively increasing month-over-month
  org: Secure and Defend Section
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: GMAU
  telemetry_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Drop in monthly users between May-July
  instrumentation:
    level: 2
    reasons:
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
  sisense_data:
    chart: 9620496
    dashboard: 697792
    embed: v2
- name: Secure:Dynamic Analysis - Paid GMAU - Paid users running
    DAST
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST jobs.
  target: Progressively increasing month-over-month
  org: Secure and Defend Section
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: Paid GMAU
  telemetry_type: SaaS
  plan_type: Paid
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Drop in monthly users between May-July
  instrumentation:
    level: 2
    reasons:
    - Instrumentation in place.
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
  lessons:
    learned:
      - Internal usage was being counted in previous numbers
      - More profile options need to be introduced for on-demand DAST scans to have broad usage
  monthly_focus:
    goals:
      - Introducing Scanner profile (https://gitlab.com/gitlab-org/gitlab/-/issues/222767)
      - Increasing options for Site profile (https://gitlab.com/groups/gitlab-org/-/epics/4079)
      - DAST profile library visibility (https://gitlab.com/gitlab-org/gitlab/-/issues/241263)
  sisense_data:
    chart: 9620496
    dashboard: 697792
    embed: v2
- name: Secure:Composition Analysis - GMAU - Users running any
    SCA scanners
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Container
    Scanning jobs, number of unique users who have run one or more Dependency Scanning
    jobs, and number of unique users who have run one or more License Scanning jobs.
    Same as Paid GMAU.
  target: Progressively increasing month-over-month, >2%
  org: Secure and Defend Section
  stage: secure
  group: composition_analysis
  public: true
  pi_type: GMAU
  telemetry_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  instrumentation:
    level: 2
    reasons:
    - We tend to have timeout issues with the data database and are having problems counting at least [License Compliance](https://gitlab.com/gitlab-org/gitlab/-/issues/202124) when using [this model](https://gitlab.com/gitlab-org/gitlab/-/issues/211621).
    - May need to rework our [new planned job analytics](https://gitlab.com/groups/gitlab-org/-/epics/3952) completely [pending a discussion with data team](https://gitlab.com/gitlab-org/gitlab/-/issues/242240#note_404921519).
    - We may be sending [too much data](https://gitlab.com/gitlab-org/gitlab/-/issues/219334) and have been asked to rethink that.
    - We only count successful job runs not [failed](https://gitlab.com/gitlab-org/gitlab/-/issues/241201) ones.
    - Can't differentiate [between the same user running multiple job types](https://gitlab.com/gitlab-org/gitlab/-/issues/230982), so possible
      double counting.
    - Chart is for .com-only data while usage ping becomes more stable for on-premise. [Issue1](https://gitlab.com/gitlab-org/gitlab/-/issues/229618#note_381037115) and [Issue2](https://gitlab.com/gitlab-org/gitlab/-/issues/231263).
  lessons:
    learned:
      - Continuing to learn about how to implement telemetry, learned what iglu was.
  monthly_focus:
    goals:
      - Continuing to try and move forward with our first [SaaS PI - number of findings](https://gitlab.com/groups/gitlab-org/-/epics/3952) which we have been working for 3+ releases.
  sisense_data:
    chart: 9284790
    dashboard: 707777
    embed: v2
- name: Secure:Composition Analysis - Paid GMAU - Paid users running
    any SCA scanners
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Container
    Scanning jobs, number of unique users who have run one or more Dependency Scanning
    jobs, and number of unique users who have run one or more License Scanning jobs.
  target: Progressively increasing month-over-month, >2%
  org: Secure and Defend Section
  stage: secure
  group: composition_analysis
  public: true
  pi_type: Paid GMAU
  telemetry_type: SaaS
  plan_type: Paid
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  instrumentation:
    level: 2
    reasons:
    - Can't differentiate between the same user running multiple job types, so possible
    - Chart is for .com-only data while usage ping becomes more stable for on-premise.
    - Can't currently [differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users](https://gitlab.com/gitlab-org/gitlab/-/issues/230981).
  lessons:
    learned:
      - Same as GMAU.
  monthly_focus:
    goals:
      - Same as GMAU.
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Fuzz Testing - GMAU - Users running fuzz testing
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Fuzz
    Testing jobs. Same as Paid GMAU.
  target: Progressively increasing month-over-month, >25%
  org: Secure and Defend Section
  stage: secure
  group: fuzz_testing
  public: true
  pi_type: GMAU
  telemetry_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 1
    reasons:
    - We need substantially more users to use fuzz testing.
  instrumentation:
    level: 2
    reasons:
    - We now have reporting on .com.
    - Usage ping still being deployed from last release. Will likely capture no results due to being keyed off of job name. Tracking work on improving this in [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/239118).
  lessons:
    learned:
      - We learned about changes needed to usage ping. Since users can set their own job names, we can't look just at job names.
      - We observed .com Snowplow working correctly, as we had several users reporting data.
  monthly_focus:
    goals:
      - Fuzz testing results in MR widget (https://gitlab.com/gitlab-org/gitlab/-/issues/210343)
      - OAuth2 authentication support (https://gitlab.com/gitlab-org/gitlab/-/issues/227493)
  sisense_data:
    chart: 9566222
    dashboard: 707777
    embed: v2
- name: Secure:Fuzz Testing - Paid GMAU - Paid users running fuzz
    testing
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The number of unique users who have run one or more Fuzz Testing jobs.
  target: Progressively increasing month-over-month, >25%
  org: Secure and Defend Section
  stage: secure
  group: fuzz_testing
  public: true
  pi_type: Paid GMAU
  telemetry_type: SaaS
  plan_type: Paid
  is_primary: true
  is_key: false
  health:
    level: 1
    reasons:
    - We need substantially more users to use fuzz testing.
  instrumentation:
    level: 2
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
    - We now have reporting on .com.
    - Usage ping still being deployed from last release. Will likely capture no results due to being keyed off of job name.
  lessons:
    learned:
      - We learned about changes needed to usage ping. Since users can set their own job names, we can't look just at job names.
      - We observed .com Snowplow working correctly, as we had several users reporting data.
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Secure:Threat Insights - GMAU - Users interacting with
    Secure UI
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report. Same as Paid GMAU until OSS projects
    can be separated.
  target: 10% month-over-month increase
  org: Secure and Defend Section
  stage: secure
  group: threat_insights
  public: true
  pi_type: GMAU
  telemetry_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - 6-month usage is mostly flat; August is 2.5% down from 6-month average
  instrumentation:
    level: 2
    reasons:
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Individual page usage up for Group and Instance dashboards, vulnerability details page but down for Project dashboards, dragging overall average down
      - MR security widget engagement is steady week-to-week, higher than Pipeline security report interaction
  monthly_focus:
    goals:
      - Adding new Vulnerability Trends chart as foundation for project-level dashboard (https://gitlab.com/gitlab-org/gitlab/-/issues/235558)
      - Alway display detected date on vulnerability details (https://gitlab.com/gitlab-org/gitlab/-/issues/222346)
  sisense_data:
    chart: 9244137
    dashboard: 707777
    embed: v2
- name: Secure:Threat Insights - Paid GMAU - Paid users interacting
    with Secure UI
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report.
  target: 10% month-over-month increase
  org: Secure and Defend Section
  stage: secure
  group: threat_insights
  public: true
  pi_type: Paid GMAU
  telemetry_type: SaaS
  plan_type: Paid
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - 6-month usage is mostly flat; August is 2.5% down from 6-month average
  instrumentation:
    level: 2
    reasons:
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Individual page usage up for Group and Instance dashboards, vulnerability details page but down for Project dashboards, dragging overall average down
      - MR security widget engagement is steady week-to-week, higher than Pipeline security report interaction
  monthly_focus:
    goals:
      - Adding new Vulnerability Trends chart as foundation for project-level dashboard (https://gitlab.com/gitlab-org/gitlab/-/issues/235558)
      - Alway display detected date on vulnerability details (https://gitlab.com/gitlab-org/gitlab/-/issues/222346)
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670
- name: Defend:Container Security - GMAC - Clusters using container
    security
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (WAF, Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing >25% month-over-month
  org: Secure and Defend Section
  stage: Defend
  group: container_security
  public: true
  pi_type: GMAC
  telemetry_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - Due to the delay in getting metrics, it is still too early to assess usage levels or trends
  instrumentation:
    level: 2
    reasons:
    - PI data is now available for WAF; however, GMAC for WAF is not available
    - GMAC data is now available for Container Network Security; however, due to the 51 day reporting delay, it is still too early to assess usage levels or trends
  lessons:
    learned:
      - WAF traffic processed is increasing.  Almost all users are using the WAF in audit mode only.
      - Metric data is still too early to draw conclusions about CNS.  The current reported data is likely only from our dev/test environments.  Customer reported data is expected to begin coming in next month.
  monthly_focus:
    goals:
      - Create, Edit, and Delete Policies (https://gitlab.com/groups/gitlab-org/-/epics/3403)
  sisense_data:
    chart: 9620410
    dashboard: 694854
    embed: v2
- name: Defend:Container Security - Paid GMAC - Paid clusters using
    container security
  base_path: "/handbook/product/secure-and-defend-section-performance-indicators/"
  definition: The number of active, paid clusters with at least one Container Security
    feature enabled (WAF, Network Policies, or Host Security) in the last 28 days.
  target: Progressively >25% increasing month-over-month
  org: Secure and Defend Section
  stage: defend
  group: container_security
  public: true
  pi_type: Paid GMAU
  telemetry_type: Both
  plan_type: Paid
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - Due to the delay in getting metrics, it is still too early to assess usage levels or trends
  instrumentation:
    level: 2
    reasons:
    - PI data is now available for WAF; however, Paid GMAC for WAF is not available
    - Paid GMAC data is now available for Container Network Security; however, due to the 51 day reporting delay, it is still too early to assess usage levels or trends
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid
      Gold/Ultimate users.
  lessons:
    learned:
      - WAF traffic processed is increasing.  Almost all users are using the WAF in audit mode only.
      - Metric data is still too early to draw conclusions about CNS.  The current reported data is likely only from our dev/test environments.  Customer reported data is expected to begin coming in next month.
  monthly_focus:
    goals:
      - Create, Edit, and Delete Policies (https://gitlab.com/groups/gitlab-org/-/epics/3403)
  sisense_data:
    chart: 9620467
    dashboard: 694854
    embed: v2


