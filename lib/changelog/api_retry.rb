# frozen_string_literal: true

module ApiRetry
  # API calls can fail to connect occasionally, which really shouldn't be a reason to fail entirely
  # Give them 5 goes, just to get past any transient network fail
  def api_retry
    # This list of http_errors was taken from Bundler::Fetcher, excluding Bundler::PersistentHTTP::Error
    http_errors = [
      Timeout::Error, EOFError, SocketError, Errno::ENETDOWN, Errno::ENETUNREACH,
      Errno::EINVAL, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EAGAIN,
      Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError,
      Zlib::BufError, Errno::EHOSTUNREACH
    ]

    tries = 0
    begin
      yield
    rescue *http_errors => error # There may be more errors we should catch
      puts "Received a known retriable exception #{error.class}; have failed #{tries} times so far"
      retry if (tries += 1) < 5
      puts "Giving up after 5 retries"
      raise error
    # rubocop:disable Style/RescueStandardError
    rescue => error # Some other error: log the class but don't retry; we can investigate later
      puts "Received an #{error.class} exception; not retrying"
      raise error
    end
    # rubocop:enable Style/RescueStandardError
  end
end
